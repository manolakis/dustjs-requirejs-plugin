# DustJS RequireJS Plugin

This plugin provides [Dust.js](http://akdubya.github.com/dustjs/) templates through [RequireJS](http://requirejs.org).

It is also compatible with [RequireJS optimization tool](http://requirejs.org/docs/optimization.html) with
[NodeJS](http://nodejs.org/). It has not been tested on Java.

## Dependencies

You must install Dust through npm in order to execute the optimization tool.

```
npm install -g dust
```

## Use

```javascript
define([
    'dustjs!template-name'
], function(template) {
    template.render(template-context, function(err, res) {
        ...
    });
}
```

Note that now is not neccesary to have loaded the [Dust core library](http://akdubya.github.com/dustjs/#guide) in your page to reder
the compiled templates.

```html
<script src="dust-core-0.3.0.min.js"></script>
```
You can remove that link! :)

## License
Available via the MIT or new BSD license.